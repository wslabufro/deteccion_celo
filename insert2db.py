import time, datetime
import sys
from threading import Timer
import MySQLdb as mdb
from itertools import islice

temp = 'registro.log'
limit_path = 'limite.dat'
today = datetime.date.today()
reg_path = today.strftime('%d%m%Y.log')

def getFrame(dataframe):

#    fecha_hora = int(dataframe[1:5].encode('hex'), 16)
#    id_dispo = int(dataframe[5:13].encode('hex'), 16)
#    temp = (int(dataframe[14:16].encode('hex'), 16) / float(16))
    cmd = str(dataframe[0].encode('hex'))
    if cmd == 'aa':

### Extraccion de datos frame lento####
    #comando
    
    # fecha y hora
        fecha_hora = struct.unpack('>I',dataframe[1:5])[0]
    # ID dispositivo (mac radio)
        id_dispo = struct.unpack('>Q',dataframe[5:13])[0]
    # Version del HW
        ver_hw = int(dataframe[13].encode('hex'), 16)
    # Temperatura ds18b20
        temp = (struct.unpack('>H',dataframe[14:16])[0] / float(16))
    # estatus de la bateria, no_cargando(00), cargando (01), completo (10), error (11)
        batt_status = (int(dataframe[17].encode('hex'),16) >> 6) & 0xF
    # Medidad del volataje de la baeteria. El escalar que multiplica el dato es el factor de escala
        batt_meas = (struct.unpack('>H',dataframe[16:18])[0] & 0x0FFF) * float(0.006404296875)
    ###  GPS data ###
        latit = (struct.unpack('>f',dataframe[18:22]))
        longi = (struct.unpack('>f',dataframe[22:26]))
        vel = (struct.unpack('>f',dataframe[26:30]))
        curso = (struct.unpack('>f',dataframe[30:34]))
        n_sat = struct.unpack('>H',dataframe[34:36])[0]
        hdop = struct.unpack('>h',dataframe[36:38])[0]

    # Comprobacion de datos    crc = int(dataframe[38].encode('hex'), 16)

        crc = int(dataframe[38].encode('hex'), 16)

        print (cmd)
        print (datetime.datetime.fromtimestamp(fecha_hora).strftime('%Y-%m-%d %H:%M:%S'))
        print ("ID dispositivo %x" % id_dispo)
        print ("Ver: %d" % ver_hw)
        print ("Temperature: %.3f" % temp)
        print ("Batt status: %d" % batt_status)
        print ("Batt measure (V): %.2f" % (batt_meas))  
        print ("Latitud: %s" % latit)

### Extraccion de datos frame rapido####
    elif cmd == 'ab':
        fecha_hora = struct.unpack('>I',dataframe[1:5])[0]
    # ID dispositivo (mac radio)
        id_dispo = struct.unpack('>Q',dataframe[5:13])[0]
    # Version del HW
        ver_hw = int(dataframe[13].encode('hex'), 16)
### Variables IMU ###
    # Acelerometro
        accelx = struct.unpack('>h',dataframe[14:16])[0]
        accely = struct.unpack('>h',dataframe[16:18])[0]
        accelz = struct.unpack('>h',dataframe[18:20])[0]
    # Giroscopio
        gyrox = struct.unpack('>h',dataframe[20:22])[0]
        gyroy = struct.unpack('>h',dataframe[22:24])[0]
        gyroz = struct.unpack('>h',dataframe[24:26])[0]
    # Magnetometro
        magx = struct.unpack('>h',dataframe[26:28])[0]
        magy = struct.unpack('>h',dataframe[28:30])[0]
        magz = struct.unpack('>h',dataframe[30:32])[0]
    # Fused quaternion
        fq1 = struct.unpack('>f',dataframe[32:36])[0]
        fq2 = struct.unpack('>f',dataframe[36:40])[0]
        fq3 = struct.unpack('>f',dataframe[40:44])[0]
        fq4 = struct.unpack('>f',dataframe[44:48])[0]

    # Audio
        audio = struct.unpack('>h',dataframe[48:50])[0]
    # CRC
        crc = int(dataframe[38].encode('hex'), 16)

        print (cmd)
        print (datetime.datetime.fromtimestamp(fecha_hora).strftime('%Y-%m-%d %H:%M:%S'))
        print ("ID dispositivo %x" % id_dispo)
        print ("Ver: %d" % ver_hw)



def insert_reg(dataframe):

	cur = con.cursor()
	try:
		cur.execute("INSERT INTO registros(n_ins, hora_fecha, a, b) VALUES(NULL,NOW(),%s,%s)" % (dataframe[0],dataframe[1]))
		con.commit()

	except mdb.IntegrityError:
		print 'Error al insertar los valores'

	finally:
		cur.close()

def main_loop():	
# Obtener la fila DESDE donde se realizara el insert
	try:
    		with open(limit_path) as limit:
			l_inf = limit.read()
			limit.close()
	except IOError:
	# Si no existe, crea el archivo
		#with open(limit_path, 'w+') as limit:
		#	limit.write('0')
			l_inf = 0
			print 'Limite inferior 0'



	# Obtener la fila HASTA donde se realizara el insert
	with open(reg_path,'rb') as reg:
		l_sup = sum(1 for n_reg in reg if n_reg != '\n')
		print int(l_inf)
		print int(l_sup)
		if  int(l_inf) >= int(l_sup):
			print 'No hay registros nuevos que enviar'

		else:
			reg.seek(0)
        # realizar cambios para insert de datos.                
	# Realizar el insert de los nuevos datos
			for line in islice(reg, int(l_inf), int(l_sup)):
				currentline = line
				insert_reg(currentline)

	# Guardar la ultima fila enviada
			with open(limit_path, 'w') as limit:
				limit.write(str(l_sup))
				limit.close()

	reg.close() 
	con.close()

try:  
    con = mdb.connect('146.83.206.92', 'wslab', 'ufro', 'playground');
    print "Intentando comunicarse con servidor remoto"
    if con:
        main_loop()		

except mdb.Error,e:
    print "Error"
    sys.exit(1)
        
finally:
    sys.exit()          
