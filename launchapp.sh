#!/bin/bash
# launchapp.sh
# Aplicacion que supervisa la ejecucion de serial2save.py
dir='/var/run'
file="$dir/serialsave.pid"
if [ -f "$file" ]
then
	pid=$(cat /var/run/serialsave.pid)
	echo "$pid"
	if [ -d "/proc/$pid" ]
	then 
		echo "$file Aplicacion corriendo"
	else
		rm -r $file
		/etc/init.d/serialsave restart
		echo "Aplicacion reiniciada"
	fi
else
	echo "$pid Aplicacion iniciada"
	/etc/init.d/serialsave start
fi

