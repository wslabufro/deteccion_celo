#!/usr/bin/python
# Obtiene y guarda los dataframes enviados por el nodo final hacia el concentrador de datos a traves de la puerta serial
from __future__ import print_function
import serial, io, struct

addr  = '/dev/ttyUSB0'  # serial port to read data from
baud  = '115200'            # baud rate for serial port
fname = 'registro.log'   # log file to save data in
fmode = 'ab'            #log file mode = append

def find_start():
    cur = 0
    print ("Buscando inicio")
    while cur != '\n':
        cur = spb.read(1)

def crc_check(ini, datafr):
    for i in datafr:    
        ini  = ini ^ ord(i)
    return ini

def getFrame(cmd,dataframe):

    if cmd == 'aa':

        ### Extraccion de datos frame lento####
    
        # fecha y hora
        fecha_hora = struct.unpack('>I',dataframe[:4])[0]
        # ID dispositivo (mac radio)
        id_dispo = struct.unpack('>Q',dataframe[4:12])[0]
        # Version del HW
        ver_hw = int(dataframe[12].encode('hex'), 16)
        # Temperatura ds18b20
        temp = (struct.unpack('>H',dataframe[13:15])[0] / float(16))
        # estatus de la bateria, no_cargando(00), cargando (01), completo (10), error (11)
        batt_status = (int(dataframe[16].encode('hex'),16) >> 6) & 0xF
        # Medidad del volataje de la baeteria. El escalar que multiplica el dato es el factor de escala
        batt_meas = (struct.unpack('>H',dataframe[15:17])[0] & 0x0FFF) * float(0.006404296875)
        ###  GPS data ###
        latit = (struct.unpack('>f',dataframe[17:21]))[0]
        longi = (struct.unpack('>f',dataframe[21:25]))[0]
        vel = (struct.unpack('>f',dataframe[25:29]))[0]
        curso = (struct.unpack('>f',dataframe[29:33]))[0]
        n_sat = struct.unpack('>H',dataframe[33:35])[0]
        hdop = struct.unpack('>h',dataframe[35:])[0]

	outf.write(str(cmd) + ','+ str(fecha_hora)+ ','+ str(id_dispo)+ ','+ str(ver_hw)+ ',' + str(temp) + ',' + str(batt_status) + ',' + str(batt_meas) + ',' + str(latit) + ','+ str(longi) + ',' + str(vel) + ',' + str(curso) + ',' + str(n_sat) + ',' + str(hdop) + '\r\n')
        outf.flush()
               
        print (cmd)
        print (fecha_hora)   
        print ("ID dispositivo %x" % id_dispo)
        print ("Ver: %d" % ver_hw)
        print ("Temperature: %.3f" % temp)
        print ("Batt status: %d" % batt_status)
        print ("Batt measure (V): %.2f" % (batt_meas))  
        print ("Latitud: %s" % latit)

    elif cmd == 'ab':

        fecha_hora = struct.unpack('>I',dataframe[:4])[0]
        # ID dispositivo (mac radio)
        id_dispo = struct.unpack('>Q',dataframe[4:12])[0]
	# Version del HW
    	ver_hw = int(dataframe[12].encode('hex'), 16)
        ### Variables IMU ###
    	# Acelerometro
        accelx = struct.unpack('>h',dataframe[13:15])[0]
    	accely = struct.unpack('>h',dataframe[15:17])[0]
        accelz = struct.unpack('>h',dataframe[17:19])[0]
    	# Giroscopio
        gyrox = struct.unpack('>h',dataframe[19:21])[0]
    	gyroy = struct.unpack('>h',dataframe[21:23])[0]
        gyroz = struct.unpack('>h',dataframe[23:25])[0]
    	# Magnetometro
	magx = struct.unpack('>h',dataframe[25:27])[0]
	magy = struct.unpack('>h',dataframe[27:29])[0]
	magz = struct.unpack('>h',dataframe[29:31])[0]
	# Fused quaternion
	fq1 = struct.unpack('>f',dataframe[31:35])[0]
	fq2 = struct.unpack('>f',dataframe[35:39])[0]
	fq3 = struct.unpack('>f',dataframe[39:43])[0]
	fq4 = struct.unpack('>f',dataframe[43:47])[0]

	# Audio
        audio = struct.unpack('>h',dataframe[47:])[0]

	print (cmd)
	print (fecha_hora)
	print ("ID dispositivo %x" % id_dispo)
	print ("Ver: %d" % ver_hw)

	outf.write(str(cmd) + ','+ str(fecha_hora)+ ','+ str(id_dispo)+ ','+ str(ver_hw)+ ',' + str(accelx) + ',' + str(accely) + ',' + str(accelz) + ',' + str(gyrox) + ','+ str(gyroy) + ',' + str(gyroz) + ',' + str(magx) + ',' + str(magy) + ',' + str(magz) + ',' + str(fq1) + ',' +str(fq2) + ',' + str(fq3) + ',' + str(fq4) + ',' + str(audio) + '\r\n')
        outf.flush()

with serial.Serial(addr,baud) as pt, open(fname,fmode) as outf:
    spb = io.BufferedRWPair(pt,pt,1)
    find_start()
    
    while True:       
        comando = str(spb.read(1).encode('hex'))
        print ("Comando: " + comando)
    
        if comando == 'aa':
            print ("process short data")
            data_short = spb.read(37)
            crc_rcv = int(spb.read(1).encode('hex'),16)
            endChar = spb.read(2)                 # Lectrura de los dos caracteres finales
            crc = crc_check(int(comando,16),data_short)

            if crc_rcv == crc:
                print ("CRC correcto")
                spb.write(chr(0))
                spb.flush()
                getFrame(comando, data_short)

            else :
                print ("CRC incorrecto")
                spb.write(chr(1))
                spb.flush()


        elif comando == 'ab':
            print ("process long data")
            data_long = spb.read(49)
            crc_rcv = int(spb.read(1).encode('hex'),16)
            values = spb.read(2)                 # Leo los dos caracteres finales
            crc = crc_check(int(comando,16),data_long)

            if crc_rcv == crc:
                print ("CRC correcto")
                spb.write(chr(0))
                spb.flush()
                getFrame(comando, data_long)

            else :
                print ("CRC incorrecto")
                pt.write(chr(1))
                spb.flush()
                
        else:
            find_start()

